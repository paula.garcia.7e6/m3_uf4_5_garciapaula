class Rainbow{
    val colors = listOf<String>("red","orange","yellow","green","blue","indigo","violet")
    fun isInTheRainbow(input:String): Boolean {
        if (input in colors) {
            println("true")
            return true
        } else println("false")
        return false
    }
}
fun main() {
    val rainbowColors = Rainbow()
    val input = readln()
    rainbowColors.isInTheRainbow(input)
}