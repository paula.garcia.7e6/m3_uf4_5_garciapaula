open class Question(val statement: String, val response: String) {
    var correct: Boolean = false
    fun showQuestion() {
        println(statement)
    }
    fun reviewQuestion(input: String) {
        if (input == response) {
            this.correct = true
        }
    }
    open fun showChoices() { /*Iniciando función*/ }
}
class MultipleChoiseQuestion(statement: String, response: String,
                       val choise1: String,
                       val choise2: String,
                       val choise3: String): Question(statement, response) {
    val listChoise = listOf(response,choise1,choise2,choise3).shuffled()
    val listChoiseRandom = listChoise.shuffled()
    override fun showChoices() {
        listChoiseRandom.forEach { println(it) }
        print("Escoge respuesta: ")
    }
}
class FreeTextQuestion(statement: String, response: String): Question(statement, response) {
    override fun showChoices() {
        print("Escribe la respuesta: ")
    }
}

class Quiz(val questions: MutableList<Question>) {
    fun showAllQuestions() {
        for (i in questions.indices) {
            questions[i].showQuestion()
            questions[i].showChoices()
            val input = readln()
            questions[i].reviewQuestion(input)
        }
    }
    fun showResults() {
        println("El resultado de las preguntas és... ")
        for (i in questions.indices) {
            if (questions[i].correct) {
                println("Pregunta $i ¡És correcta!")
            } else println("Pregunta $i No és correcta...")
        }
    }
}

fun main() {
    val pregunta1 = MultipleChoiseQuestion("La capital de Corea del Norte es:", "Pyongyang", "Seul", "Pekin", "Tokio")
    val pregunta2 = FreeTextQuestion("¿De los siguientes cuál es el planeta más caliente?", "Venus")
    val preguntas = mutableListOf(pregunta1, pregunta2)
    val quiz = Quiz(preguntas)
    quiz.showAllQuestions()
    quiz.showResults()
}
