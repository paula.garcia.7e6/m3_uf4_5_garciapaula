import java.util.*

open class GymControlReader() {
    var userid: String = ""
    open fun nextId():String { return "" /*Iniciamos función */ }
}

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader() {
    override fun nextId():String {
        userid = scanner.next()
        return userid
    }
}

fun main() {
    val reader = GymControlManualReader()
    val enteredUsers = mutableSetOf<String>()
    for (i in 1..8) {
        val id = reader.nextId()
        if (enteredUsers.contains(id)) {
            enteredUsers.remove(id)
            println("$id ha sortit")
        } else {
            enteredUsers.add(id)
            println("$id ha entrat")
        }
    }
}