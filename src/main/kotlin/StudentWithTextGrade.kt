class Student(var name: String, var textGrade: String) {
    fun info():String {
        return "name='${this.name}', textGrade=${this.textGrade}"
    }
}

fun main(args: Array<String>) {
    var student1 = Student("Mar","FAILED")
    var student2 = Student("Joan","EXCELLENT")
    println("""
        ${student1.info()}
        ${student2.info()}
    """.trimIndent())
}