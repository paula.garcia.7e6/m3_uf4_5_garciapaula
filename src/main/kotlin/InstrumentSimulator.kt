open class Instrument {
    open fun makeSounds(times:Int) {}
}
class Drump(val tone:String): Instrument() {
    override fun makeSounds(times: Int) {
        for  (i in 1..times) {
            when (tone) {
                "A" -> println("TAAAM")
                "O" -> println("TOOOM")
                "U" -> println("TUUUM")
                else -> println("Invalid tone")
            }
        }
    }
}
class Triangle(val resonance: Int): Instrument() {
    override fun makeSounds(times: Int) {
        for  (i in 1..times) {
            when (resonance) {
                1 -> println("TINC")
                2 -> println("TIINC")
                3 -> println("TIIINC")
                4 -> println("TIIIINC")
                5 -> println("TIIIIINC")
                else -> println("Invalid resonance")
            }
        }
    }
}
fun main(args: Array<String>) {
    fun play(instruments: List<Instrument>) {
        for (instrument in instruments) {
            instrument.makeSounds(2) // plays 2 times the sound
        }
    }
    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Drump("A"),
        Drump("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)
}